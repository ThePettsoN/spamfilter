local Core = SpamFilterCore
local SpamFilterBlockDuplicates = {
	DEBUG = Core.DEBUG or false,
	debug_printf = Core.debug_printf
}

local UPDATE_INTERVAL = 30

Core.ChatFilter = SpamFilterBlockDuplicates
local logger

local filter_map = {
	filter_say = "CHAT_MSG_SAY",
	filter_yell = "CHAT_MSG_YELL",
	filter_whisper = "CHAT_MSG_WHISPER",
	filter_party = "CHAT_MSG_PARTY",
	filter_raid = "CHAT_MSG_RAID",
	filter_guild = "CHAT_MSG_GUILD",
}

local LOG_REASON = {
	DUPLICATE = "Duplicate Message"
}
local firstFrame = getglobal("ChatFrame1").name
function log(frame, message)
	if frame.name ~= firstFrame then
		return
	end

	logger:log(message, LOG_REASON.DUPLICATE)
end

function SpamFilterBlockDuplicates:OnInitialize()
	if self._initialized then
		for key, value in pairs(filter_map) do
			ChatFrame_RemoveMessageEventFilter(value, self.OnFilterChatMessage)
		end
		ChatFrame_RemoveMessageEventFilter("CHAT_MSG_CHANNEL", self.OnFilterChannelChatMessage)

		if self._updateTimer then
			Core:CancelTimer(self._updateTimer)
			self._updateTimer = nil
		end
	end

	local profile = Core.profile
	if not profile.duplicate_messages.block_messages then
		return
	end

	for key, value in pairs(filter_map) do
		ChatFrame_AddMessageEventFilter(value, self.OnFilterChatMessage)
	end

	ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL", self.OnFilterChannelChatMessage)

	logger = Core.Logger
	self._duplicateMessages = {}
	self._updateTimer = Core:ScheduleRepeatingTimer(self._updateDuplicateMessages, UPDATE_INTERVAL, self)
	self._initialized = true
end

function SpamFilterBlockDuplicates:OnOptionsChanged()
	self:OnInitialize()
end

function SpamFilterBlockDuplicates:OnProfileChanged()
	self:OnOptionsChanged()
end

function SpamFilterBlockDuplicates.OnFilterChatMessage(frame, event, message, player, language, _, playerName)
	if playerName == Core.player_name then
		return false
	end

	local self = SpamFilterBlockDuplicates
	return self:_is_duplicate_message(playerName, message, event, frame)
end

function SpamFilterBlockDuplicates.OnFilterChannelChatMessage(frame, event, message, player, b, c, playerName, d, e, f, channelName)
	if playerName == Core.player_name then
		return false
	end

	local self = SpamFilterBlockDuplicates
	return self:_is_duplicate_message(playerName, message, channelName, frame)
end

function SpamFilterBlockDuplicates:_updateDuplicateMessages()
	local currentTime = GetTime()

	local duplicateMessages = self._duplicateMessages
	for player, data in pairs(duplicateMessages) do
		if currentTime >= (data.timestamp + Core.profile.duplicate_messages.time_count_as_duplicate) then
			self._duplicateMessages[player] = nil
			self:debug_printf("Removing message from '%s'", player)
		end
	end
end

function SpamFilterBlockDuplicates:_is_duplicate_message(player, message, channelName, frame)
	if not self:enabled() then
		return false
	end

	local currentTime = GetTime() -- Not very precise. But the best we can get

	local data = self._duplicateMessages[player]
	if not data then
		-- No existing data. Store it and return not duplicate
		self._duplicateMessages[player] = {
			message = message,
			timestamp = currentTime,
			channel = channelName,
			frameName = frame.name,
		}

		self:debug_printf("New message '%s' from '%s'", message, player)
		return false
	end

	local diff = currentTime - data.timestamp
	if diff <= 0 and data.channel == channelName and frame.name ~= data.frameName then
		-- No time diff and same channel. This is the same message being send to all different messages
		return false
	end

	if message ~= data.message then
		-- Have data from player but a different message
		-- Store the new instead
		self._duplicateMessages[player] = {
			message = message,
			timestamp = currentTime,
			channel = channelName,
		}
		self:debug_printf("Not duplicate message '%s' from '%s'", message, player)
		return false
	end

	if diff >= Core.profile.duplicate_messages.time_count_as_duplicate then
		-- Time since the first message was sent is larger than what we count as a duplicate
		self:debug_printf("Not a duplicate message '%s' anymore from '%s'", message, player)
		self._duplicateMessages[player] = nil
		return false
	end

	-- Duplicate
	self:debug_printf("Duplicate message '%s' from '%s'", message, player)
	log(frame, message)

	return true
end

function SpamFilterBlockDuplicates:enabled()
	return Core.profile.duplicate_messages.block_messages
end

Core:RegisterModule("block_duplicates", SpamFilterBlockDuplicates)