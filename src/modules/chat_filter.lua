local Core = SpamFilterCore
local SpamFilterChatFilter = {
	DEBUG = Core.DEBUG or false,
	debug_printf = Core.debug_printf
}

Core.ChatFilter = SpamFilterChatFilter
local logger

local filter_map = {
	filter_say = "CHAT_MSG_SAY",
	filter_yell = "CHAT_MSG_YELL",
	filter_whisper = "CHAT_MSG_WHISPER",
	filter_party = "CHAT_MSG_PARTY",
	filter_raid = "CHAT_MSG_RAID",
	filter_guild = "CHAT_MSG_GUILD",
}
function SpamFilterChatFilter:OnInitialize()
	self:_migrateSettings()

	if self._initialized then
		for key, value in pairs(filter_map) do
			ChatFrame_RemoveMessageEventFilter(value, self.OnFilterChatMessage)
		end
		ChatFrame_RemoveMessageEventFilter("CHAT_MSG_CHANNEL", self.OnFilterChannelChatMessage)
	end

	local chat_filter = Core.profile.chat_filter
	for key, value in pairs(filter_map) do
		if chat_filter[key] then
			ChatFrame_AddMessageEventFilter(value, self.OnFilterChatMessage)
		end
	end

	if next(chat_filter.filter_channels) ~= nil then
		ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL", self.OnFilterChannelChatMessage)
	end

	logger = Core.Logger
	self._initialized = true
end

function SpamFilterChatFilter:OnOptionsChanged()
	self:OnInitialize()
end

function SpamFilterChatFilter:OnProfileChanged()
	self:OnOptionsChanged()
end

local LOG_REASON = {
	BLACKLIST = "Blacklist Player",
	FILTER = "Spam Filter",
}

local firstFrame = getglobal("ChatFrame1").name
function log(frame, message, reason)
	if frame.name ~= firstFrame then
		return
	end

	logger:log(message, LOG_REASON.FILTER)
end

function SpamFilterChatFilter.OnFilterChatMessage(frame, channelName, message, player, _, _, playerName)
	if playerName == Core.player_name then
		return false
	end

	local self = SpamFilterChatFilter

	if self:_whitelistedPlayer(playerName) then
		return false -- Player is whitelisted. Let all messages from player through
	end

	if self:_blacklistedPlayer(playerName) then
		log(frame, message, LOG_REASON.FILTER)
		return true -- Player is blacklisted. Block all messages
	end

	if self:_filterMessage(message) then
		log(frame, message, LOG_REASON.FILTER)
		return true
	end

	return false
end

function SpamFilterChatFilter.OnFilterChannelChatMessage(frame, b, message, player, c, d, playerName, e, f, g, channelName)
	if playerName == Core.player_name then
		return false
	end

	local self = SpamFilterChatFilter

	-- Convert zone specific channels into their generic counterpart
	-- Custom channels can not include whitespace so this should be fine
	channelName = string.gsub(channelName, " .*", "")

	local filterChannels = Core.profile.chat_filter.filter_channels
	if filterChannels[channelName] == nil then
		return false -- Channel not being filtered. Let it through
	end

	if self:_whitelistedPlayer(playerName) then
		return false -- Player is whitelisted. Let all messages from player through
	end

	if self:_blacklistedPlayer(playerName) then
		log(frame, message, LOG_REASON.FILTER)
		return true -- Player is blacklisted. Block all messages
	end

	if self:_filterMessage(message) then
		log(frame, message, LOG_REASON.FILTER)
		return true
	end

	return false
end

function SpamFilterChatFilter:_whitelistedPlayer(playerName)
	playerName = string.lower(playerName)

	local whitelistedPlayers = Core.profile.chat_filter.whitelist
	for _, whitelistedPlayer in pairs(whitelistedPlayers) do
		if playerName == whitelistedPlayer then
			self:debug_printf("Player '%s' whitelisted", playerName)
			return true
		end
	end

	self:debug_printf("Player '%s' not whitelisted", playerName)
	return false
end

function SpamFilterChatFilter:_blacklistedPlayer(playerName)
	playerName = string.lower(playerName)

	local blacklistedPlayers = Core.profile.chat_filter.blacklist
	for _, blacklistedPlayer in pairs(blacklistedPlayers) do
		if playerName == blacklistedPlayer then
			self:debug_printf("Player '%s' blacklisted", playerName)
			return true
		end
	end

	self:debug_printf("Player '%s' not blacklisted", playerName)
	return false
end

function SpamFilterChatFilter:_filterMessage(message)
	message = string.lower(message)

	local filterStrings = Core.profile.chat_filter.filters
	for _, filter in pairs(filterStrings) do
		if string.find(message, filter) then
			self:debug_printf("Message '%s' filtered", message)
			return true
		end
	end

	self:debug_printf("Message '%s' not filtered", message)
	return false
end

function SpamFilterChatFilter:_migrateSettings()
	local profile = Core.profile
	local version = profile.version
	local defaults = Core.db.defaults.profile

	local deepcopy = Core.Utils.tabledeepcopy

	-- Migrate to new database structure
	if not version or version.major < 2 then
		local chat_filter = profile.chat_filter or {}

		chat_filter.filters = deepcopy(profile.chat_msg_filters) or defaults.chat_filter.filters
		chat_filter.blacklist = deepcopy(profile.blacklisted_players) or defaults.chat_filter.blacklist
		chat_filter.whitelist = deepcopy(profile.whitelisted_players) or defaults.chat_filter.whitelist
		chat_filter.filter_channels = deepcopy(profile.filter_channels) or defaults.chat_filter.filter_channels

		for key, _ in pairs(filter_map) do
			chat_filter[key] = deepcopy(profile[key]) or defaults.chat_filter[key]
			profile[key] = nil
		end

		profile.chat_msg_filters = nil
		profile.blacklisted_players = nil
		profile.whitelisted_players = nil
		profile.filter_channels = nil

		profile.chat_filter = chat_filter
	end
end

Core:RegisterModule("chat_filter", SpamFilterChatFilter)