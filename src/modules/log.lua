local Core = SpamFilterCore
local SpamFilterLogger = {
	DEBUG = Core.DEBUG or false,
	debug_printf = Core.debug_printf
}
local AceConfigDialog = LibStub("AceConfigDialog-3.0")
local AceConfigRegistry = LibStub("AceConfigRegistry-3.0")

Core.Logger = SpamFilterLogger

local filter_map = {
	filter_say = "CHAT_MSG_SAY",
	filter_yell = "CHAT_MSG_YELL",
	filter_whisper = "CHAT_MSG_WHISPER",
	filter_party = "CHAT_MSG_PARTY",
	filter_raid = "CHAT_MSG_RAID",
	filter_guild = "CHAT_MSG_GUILD",
}
function SpamFilterLogger:OnInitialize()
	if not self._initialized then
		self._log = {}
		self._log_size = 50
		self._num_entries = 0
	else
		self._num_entries = #self._log
	end

	self._initialized = true
end

function SpamFilterLogger:OnOptionsChanged()
	self:OnInitialize()
end

function SpamFilterLogger:OnProfileChanged()
	self:OnOptionsChanged()
end

function options_visible()
	return AceConfigDialog.BlizOptions.SpamFilterCore.SpamFilterCore.frame:IsVisible()
end

function SpamFilterLogger:log(message, reason)
	-- history is full
	-- We shift all elements except the last one to the right.
	-- This will remove the oldest entry, while making it possible to fill the first
	-- position efficently
	for i = (self._log_size - 1), 1, -1 do
		self._log[i + 1] = self._log[i]
	end

	self._log[1] = {message = message, reason = reason}

	if self._num_entries > self._log_size then
		self._log[#self._log] = nil
	else
		self._num_entries = self._num_entries + 1
	end

	self:count()

	if options_visible() then
		AceConfigRegistry:NotifyChange("SpamFilterCore")
	end
end

function SpamFilterLogger:count()
	Core.profile.statistics.num_blocked_messages = Core.profile.statistics.num_blocked_messages + 1
	Core.db.global.statistics.num_blocked_messages = Core.db.global.statistics.num_blocked_messages + 1
end

Core:RegisterModule("log", SpamFilterLogger)