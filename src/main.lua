local MAJOR, MINOR = 2,1

SpamFilterCore = LibStub("AceAddon-3.0"):NewAddon("SpamFilterCore", "AceEvent-3.0", "AceTimer-3.0")
SpamFilterCore.DEBUG = false
SpamFilterCore.modules = {}
SpamFilterCore.VERSION = {
	MAJOR = MAJOR,
	MINOR = MINOR
}

function SpamFilterCore:debug_printf(msg, ...)
	if self.DEBUG then
		print(string.format(msg, ...))
	end
end

function SpamFilterCore:RegisterModule(name, module)
	assert(self.modules[name] == nil, "Failed to register module: Name already exists")

	self.modules[name] = module
	self.debug_printf("New Module '%s' Registered", name)
end

function SpamFilterCore:OnInitialize()
	self.db = LibStub("AceDB-3.0"):New("SpamFilterCore_DB", self.SpamFilterDefaultDB)
	self.profile = self.db.profile
	self.player_name = UnitName("player")

	self.db.RegisterCallback(self, "OnProfileChanged", "OnProfileChanged")
	self.db.RegisterCallback(self, "OnProfileCopied", "OnProfileChanged")
	self.db.RegisterCallback(self, "OnProfileReset", "OnProfileChanged")

	for a, sub_module in pairs(self.modules) do
		sub_module:OnInitialize()
	end

	self.profile.version = {
		major = self.VERSION.MAJOR,
		minor = self.VERSION.MINOR
	}

	self.Options:OnInitialize()
	self._initialized = true
end

function SpamFilterCore:OnEnable()
	-- PLAYER_LOGIN
	if self.profile.display_login_message then
		local sessionMsg = string.format("|cffffaa11SpamFilter: %d messages filtered last session|r", self.profile.statistics.num_blocked_messages)
		local totalMsg = string.format("|cffffaa11SpamFilter: %d total messages filtered|r", self.db.global.statistics.num_blocked_messages)
		local frame = (SELECTED_CHAT_FRAME or DEFAULT_CHAT_FRAME)

		frame:AddMessage(sessionMsg)
		frame:AddMessage(totalMsg)
	end

	self.profile.statistics.num_blocked_messages = 0
end

function SpamFilterCore:OnProfileChanged()
	self.profile = self.db.profile

	for _, sub_module in pairs(self.modules) do
		sub_module:OnProfileChanged()
	end

	self.Options:BuildOptionsTable()
end

function SpamFilterCore:OnOptionsChanged()
	for _, sub_module in pairs(self.modules) do
		sub_module:OnOptionsChanged()
	end
end
