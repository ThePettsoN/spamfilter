local Utils = setmetatable({}, getmetatable(table))
SpamFilterCore.Utils = Utils

function Utils.tablesize(t)
	local count = 0
	for key, value in pairs(t) do
		count = count + 1
	end
	return count
end

function Utils.tabledeepcopy(orig)
	local orig_type = type(orig)
	local copy
	if orig_type == 'table' then
		copy = {}
		for orig_key, orig_value in next, orig, nil do
			copy[Utils.tabledeepcopy(orig_key)] = Utils.tabledeepcopy(orig_value)
		end
		setmetatable(copy, Utils.tabledeepcopy(getmetatable(orig)))
	else -- number, string, boolean, etc
		copy = orig
	end
	return copy
end
