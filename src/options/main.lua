local SpamFilterCore = SpamFilterCore
local SpamFilterOptions =  {optionTemplates = {}}
SpamFilterCore.Options = SpamFilterOptions

local LibStub = LibStub
local AceConfig = LibStub("AceConfig-3.0")
local AceConfigRegistry = LibStub("AceConfigRegistry-3.0")
local AceConfigDialog = LibStub("AceConfigDialog-3.0")
local AceDBOptions = LibStub("AceDBOptions-3.0")

function SpamFilterOptions:OnInitialize()
	self.config = {
		type = "group",
		name = "SpamFilter",
		handler = SpamFilterCore,
		args = {
		},
	}

	AceConfig:RegisterOptionsTable("SpamFilterCore", self.GetConfig)
	AceConfigDialog:AddToBlizOptions("SpamFilterCore", "SpamFilter")
end

function SpamFilterOptions:AddOptionTemplate(name, template_func)
	assert(not self.optionTemplates[name])
	self.optionTemplates[name] = template_func
end

local config_template = {
	type = "group",
	name = "SpamFilter",
	args = {},
}
function SpamFilterOptions:GetConfig()
	local args = config_template.args
	for name, func in pairs(SpamFilterOptions.optionTemplates) do
		args[name] = func()
	end

	config_template.args.profiles = AceDBOptions:GetOptionsTable(SpamFilterCore.db)
	return config_template
end
