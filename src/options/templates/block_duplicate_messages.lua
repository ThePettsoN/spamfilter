local SpamFilterCore = SpamFilterCore
local SpamFilterOptions =  SpamFilterCore.Options

local function optionsTemplate()
	local profile = SpamFilterCore.profile
	local args = {
		block_duplicate_messages = {
			order = 1,
			type = "toggle",
			width = "full",
			name = "Block Duplicate Messages",
			get = function(info) return profile.duplicate_messages.block_messages end,
			set = function(info, new_value) 
				profile.duplicate_messages.block_messages = new_value
			end
		},
		time_count_as_duplicate = {
			order = 2,
			type = "range",
			min = 1,
			max = 30,
			step = 1,
			bigStep = 1,
			-- width = "full",
			name = "Time Between Messages",
			get = function(info) return profile.duplicate_messages.time_count_as_duplicate end,
			set = function(info, new_value)
				profile.duplicate_messages.time_count_as_duplicate = new_value
			end
		},
	}

	return {
		type = "group",
		name = "Duplicate Messages",
		order = 3,
		args = args
	}
end

SpamFilterOptions:AddOptionTemplate("duplicate_messages", optionsTemplate)