local SpamFilterCore = SpamFilterCore
local SpamFilterOptions =  SpamFilterCore.Options

local function optionsTemplate()
	local profile = SpamFilterCore.profile
	local args = {
		header = {
			order = 1,
			type = "header",
			width = "full",
			name = "SpamFilter",
		},
		display_login_message = {
			order = 2,
			type = "toggle",
			width = "full",
			name = "Show Login Message",
			get = function(info)
				return profile.display_login_message
			end,
			set = function(info, new_value)
				profile.display_login_message = new_value
			end
		},
		separator = {
			order = 3,
			type = "header",
			width = "full",
			name = "",
		},
		block_desc = {
			order = 4,
			type = "description",
			width = "full",
			name = "Blocked Messages",
		},
		total = {
			order = 5,
			type = "description",
			width = "full",
			name = string.format("Total: %d", SpamFilterCore.db.global.statistics.num_blocked_messages),
		},
		session = {
			order = 6,
			type = "description",
			width = "full",
			name = string.format("Session: %d", profile.statistics.num_blocked_messages),
		},
		version = {
			order = 7,
			type = "description",
			width = "full",
			name = string.format("\n\nVersion: %d.%d", SpamFilterCore.VERSION.MAJOR, SpamFilterCore.VERSION.MINOR)
		},
	}

	return {
		type = "group",
		name = "General",
		order = 1,
		args = args
	}
end

SpamFilterOptions:AddOptionTemplate("general", optionsTemplate)