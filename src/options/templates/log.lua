local SpamFilterCore = SpamFilterCore

local options =  SpamFilterCore.Options
local logger = SpamFilterCore.Logger

local function optionsTemplate()
	local profile = SpamFilterCore.profile
	local args = {
		header = {
			order = 1,
			type = "header",
			width = "full",
			name = "Filter history",
		}
	}

	local order = 1
	for i = 1, #logger._log do
		local entry = logger._log[i]

		args["message_" .. tostring(i)] = {
			order = order + 1,
			type = "input",
			width = "full",
			name = "",
			get = function(info)
				return entry.message
			end,
			set = function(info, new_value) end,
		}

		order = order + 1
	end

	return {
		type = "group",
		name = "Log",
		order = 4,
		args = args
	}
end

options:AddOptionTemplate("log", optionsTemplate)