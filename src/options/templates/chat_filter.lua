local SpamFilterCore = SpamFilterCore
local SpamFilterOptions =  SpamFilterCore.Options

local function whitelistTemplate()
	local args = {}
	local whitelistedPlayers = SpamFilterCore.profile.chat_filter.whitelist

	local subOrder = 1
	args["new_input"] = {
		order = 1,
		type = "input",
		width = "full",
		name = "Whitelist New Player",
		get = function(info) end,
		set = function(info, new_value)
			whitelistedPlayers[#whitelistedPlayers + 1] = string.lower(new_value)
		end,
	}

	local group = {
		type = "group",
		name = "Whitelisted Players",
		order = 2,
		inline = true,
		args = {}
	}
	args["existing_players"] = group

	for key, value in pairs(whitelistedPlayers) do
		group.args[string.format("%d_input", key)] = {
			order = subOrder,
			type = "input",
			name = "",
			get = function(info) return value end,
			set = function(info, new_value)
				whitelistedPlayers[key] = string.lower(new_value)
			end,
		}
		group.args[string.format("%d_remove", key)] = {
			order = subOrder + 1,
			type = "execute",
			name = "Remove",
			func = function()
				whitelistedPlayers[key] = nil
			end,
		}
		subOrder = subOrder + 2
	end

	return {
		type = "group",
		name = "Whitelist",
		order = 1,
		args = args
	}
end

local function blacklistTemplate()
	local args = {}
	local blacklistedPlayers = SpamFilterCore.profile.chat_filter.blacklist

	local subOrder = 1
	args["new_input"] = {
		order = 1,
		type = "input",
		width = "full",
		name = "Blacklist New Player",
		get = function(info) end,
		set = function(info, new_value)
			blacklistedPlayers[#blacklistedPlayers + 1] = string.lower(new_value)
		end,
	}

	local group = {
		type = "group",
		name = "Blacklisted Players",
		order = 2,
		inline = true,
		args = {}
	}
	args["existing_players"] = group

	for key, value in pairs(blacklistedPlayers) do
		group.args[string.format("%d_input", key)] = {
			order = subOrder,
			type = "input",
			name = "",
			get = function(info) return value end,
			set = function(info, new_value)
				blacklistedPlayers[key] = string.lower(new_value)
			end,
		}
		group.args[string.format("%d_remove", key)] = {
			order = subOrder + 1,
			type = "execute",
			name = "Remove",
			func = function()
				blacklistedPlayers[key] = nil
			end,
		}
		subOrder = subOrder + 2
	end

	return {
		type = "group",
		name = "Blacklist",
		order = 2,
		args = args
	}
end

local function players(order)
	return {
		type = "group",
		name = "Players",
		order = order,
		args = {
			whitelist_group = whitelistTemplate(),
			blacklist_group = blacklistTemplate(),
		}
	}
end

local function filters(order)
	local args = {}
	local chatFilter = SpamFilterCore.profile.chat_filter
	local filters = chatFilter.filters

	-- Build a list of existing filters
	local subOrder = 1

	args["new_input"] = {
		order = 1,
		type = "input",
		width = "full",
		name = "New Filter",
		get = function(info) return "" end,
		set = function(info, new_value)
			filters[#filters + 1] = string.lower(new_value)
		end,
	}

	local group = {
		type = "group",
		name = "Existing Filters",
		order = 2,
		inline = true,
		args = {}
	}
	args["existing_filters"] = group

	for key, value in pairs(filters) do
		group.args[string.format("%d_input", key)] = {
			order = subOrder,
			type = "input",
			name = "",
			get = function(info) return value end,
			set = function(info, new_value)
				filters[key] = string.lower(new_value)
			end,
		}
		group.args[string.format("%d_remove", key)] = {
			order = subOrder + 1,
			type = "execute",
			name = "Remove",
			func = function()
				filters[key] = nil
			end,
		}
		subOrder = subOrder + 2
	end

	return {
		type = "group",
		name = "Filters",
		order = order,
	    args = args,
	}
end

local function channels(order)
	return {
		type = "group",
		name = "Channels",
		order = order,
		args = {
			filter_say = {
				order = 1,
				type = "toggle",
				width = "full",
				name = "Filter Say",
				desc = "Toggle whether or not to filter say messages.",
			},
			filter_yell = {
				order = 2,
				type = "toggle",
				width = "full",
				name = "Filter Yell",
				desc = "Toggle whether or not to filter yell messages.",
			},
			filter_whisper = {
				order = 3,
				type = "toggle",
				width = "full",
				name = "Filter Whisper",
				desc = "Toggle whether or not to filter whisper messages.",
			},
			filter_party = {
				order = 4,
				type = "toggle",
				width = "full",
				name = "Filter Party",
				desc = "Toggle whether or not to filter party messages.",
			},
			filter_raid = {
				order = 5,
				type = "toggle",
				width = "full",
				name = "Filter Raid",
				desc = "Toggle whether or not to filter raid messages.",
			},
			filter_guild = {
				order = 6,
				type = "toggle",
				width = "full",
				name = "Filter Guild",
				desc = "Toggle whether or not to filter guild messages.",
			},
			filter_channels = {
				order = 7,
				type = "multiselect",
				width = "full",
				name = "Filter Channels",
				desc = "Toggle whether or not to filter specific channels.",
				values = function()
					local channels = {GetChannelList()}
					local channelNames = {}
					for i = 1, #channels, 3 do
						local channel = channels[i + 1]
						channelNames[channel] = channel
					end
					return channelNames
				end,
				get = function(info, key)
					return SpamFilterCore.profile.chat_filter.filter_channels[key]
				end,
				set = function(info, key, value)
					SpamFilterCore.profile.chat_filter.filter_channels[key] = value or nil
				end,
			}
		},
		get = function(info)
			return SpamFilterCore.profile.chat_filter[info[#info]]
		end,
		set = function(info, value)
			SpamFilterCore.profile.chat_filter[info[#info]] = value
		end
	}
end


local function optionsTemplate()
	return {
		type = "group",
		name = "Chat Filter",
		order = 2,
		args = {
			channels = channels(1),
			players = players(2),
			filters = filters(3),
		},
	}
end

SpamFilterOptions:AddOptionTemplate("players", optionsTemplate)