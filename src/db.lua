SpamFilterCore.SpamFilterDefaultDB = {
	profile = {
		chat_filter = {
			filters = {
				".*wtb.*boost.*",
				".*wts.*boost.*",
				".*boost.*[0-9]+g[ \.]*",
				".*sell.*boost.*",
				".*buy.*boost.*",

				".*wtb.*sum.*",
				".*wts.*sum.*",
				".*sell.*summ.*",
				".*buy.*summ.*",

				".*wtb.*buff.*",
				".*wts.*buff.*",
				".*buffs.*[0-9]+g[ \.]*",
				".*sell.*buff.*",
				".*buy.*buff.*",
			},
			blacklist = {},
			whitelist = {},
			filter_say = true,
			filter_yell = true,
			filter_whisper = true,
			filter_party = false,
			filter_raid = false,
			filter_guild = false,
			filter_channels = {},
		},
		duplicate_messages = {
			block_messages = true,
			time_count_as_duplicate = 5,
		},
		display_login_message = false,
		statistics = {
			num_blocked_messages = 0,
		}
	},
	global = {
		statistics = {
			num_blocked_messages = 0
		}
	}
}