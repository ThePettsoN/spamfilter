## Interface: 20501
## Title: SpamFilter
## Author: ThePettsoN
## Version: 2.2.0
## Notes: Tool to filter chat message from any channel(s) in WoW Classic
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariables: SpamFilterCore_DB

embeds.xml
src/main.lua
src/db.lua
src/utils.lua

src/modules/chat_filter.lua
src/modules/block_duplicates.lua
src/modules/log.lua

src/options/main.lua
src/options/templates/chat_filter.lua
src/options/templates/block_duplicate_messages.lua
src/options/templates/general.lua
src/options/templates/log.lua